import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import FileUploader from './FileUploader/FileUploader';
import UseGetVideos from './services/getVideos'
import Visualisation from './Visualisation';

const App = () =>  {
  const [ids, setIds] = useState();
  const [loading, error, data] = UseGetVideos(ids);

  console.log(loading, error, data)


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h3>Youtube analyser</h3>
        <FileUploader setIds={setIds} />
        
        <h2>Videos:</h2>
        {loading && <p>loading</p>}
        <Visualisation data={data} />
      </header>
    </div>
  );
}

export default App;
