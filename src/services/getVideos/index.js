import { useState, useEffect } from 'react';
import { chunk, flatten } from 'lodash';


const constructQuery = (ids) => {
    const YOUTUBE_API_KEY = process.env.REACT_APP_YOUTUBE_API_KEY;
    console.log("ids", ids);
    const url = `https://www.googleapis.com/youtube/v3/videos?part=snippet%2Cstatistics%2Cid%2CtopicDetails&id=${ids.join('%2C')}&maxResults=20&key=${YOUTUBE_API_KEY}`;
    return url;
}


const UseGetVideos = (ids) => {
    const [loading, setLoading] = useState(true);
    const [videos, setVideos] = useState([]);
    const [error, setError] = useState();

    useEffect(() => {

        const fetchData = async () => {
            setError(null);
            setLoading(true);
       
            try {
                if (ids) {
                    // Handle api accepting max of 50 videos
                    const chunks = chunk(ids, 50);
                    
                    // Destroy youtube
                    const promises = Promise.all(chunks.map(fiftyIds => constructQuery(fiftyIds)).map(url => fetch(url)));
                    const responses = await promises;
                    const results = await Promise.all(responses.map(response => response.json()));
                    const videos = flatten(results.map(result => result.items));
                    setVideos(videos);
                } else {
                    setError(null);
                    setVideos([]);
                }
              
            } catch (error) {
              setError(error);
            }
       
            setLoading(false);
        };
       
        fetchData();

    }, [ids]);

    return [loading, error, videos];
} 

export default UseGetVideos;
