import React from 'react';
import { countBy } from 'lodash';
import Categories from '../Categories';

const Visualisation = (props) => {

    const { data } = props;

    if (!data || data === undefined) {
        return null;
    }

    const categories = countBy(
        data.map(video => video?.topicDetails?.topicCategories).flat().filter(topic => topic !== undefined)
    );
    console.log('categories', categories);

    console.log('Visualisaion');
    console.log(data);
    console.log(data.length)

    return (
        <div>
            <Categories categories={categories} />
            {data && data.length > 0 && data.map(v => (
                <div>
                    <p>{v.snippet.title}</p>
                    {v.topicDetails && v.topicDetails.topicCategories.map(category => (
                        <p>{category}</p>
                    ))}
                </div>
            )  
            )}
        </div>
    )

}

export default Visualisation;