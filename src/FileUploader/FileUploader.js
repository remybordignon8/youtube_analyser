import React from 'react';

const FileUploader = (props) => {

    const { setIds } = props;

    const handleFileUpload = e => {
        const fileReader = new FileReader();
        fileReader.readAsText(e.target.files[0], "application/json");
        fileReader.onload = e => {
          console.log(e.target.result);
          const file = JSON.parse(e.target.result);
          console.log(file?.map(v => v.titleUrl))
          const ids = file?.map(v => v?.titleUrl?.replace("https://www.youtube.com/watch?v\u003d", ""))

          setIds(ids);
        };
    };

    return (
        <div>
            <input type="file" onChange={handleFileUpload}/> 
        </div>
    );

}

export default FileUploader;
